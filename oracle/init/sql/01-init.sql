--------------------------------------------------------
--  文件已创建 - 星期三-十二月-26-2018   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table BANK_LOGIN_LOG
--------------------------------------------------------

  CREATE TABLE "SYSTEM"."BANK_LOGIN_LOG" 
   (	"ID" NUMBER, 
	"BANK_ID" NUMBER, 
	"USER_ID" NUMBER, 
	"NAME" VARCHAR2(100 BYTE), 
	"LOG_DATE" DATE, 
	"CLIENT_IP" VARCHAR2(20 BYTE), 
	"STATUS" NUMBER(2,0)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;

SET DEFINE OFF;
Insert into SYSTEM.BANK_LOGIN_LOG (ID,BANK_ID,USER_ID,NAME,LOG_DATE,CLIENT_IP,STATUS) values (10000,23,44,'鹿城农商银行',to_date('2018-11-11','yyyy-mm-dd'),'0:0:0:0:0:0:0:1',1);
Insert into SYSTEM.BANK_LOGIN_LOG (ID,BANK_ID,USER_ID,NAME,LOG_DATE,CLIENT_IP,STATUS) values (10003,23,44,'鹿城农商银行',to_date('2018-11-11','yyyy-mm-dd'),'123.159.201.110',1);
Insert into SYSTEM.BANK_LOGIN_LOG (ID,BANK_ID,USER_ID,NAME,LOG_DATE,CLIENT_IP,STATUS) values (394,1,22,'中国农业银行',to_date('2018-11-11','yyyy-mm-dd'),'10.119.130.250',1);
Insert into SYSTEM.BANK_LOGIN_LOG (ID,BANK_ID,USER_ID,NAME,LOG_DATE,CLIENT_IP,STATUS) values (395,1,22,'中国农业银行',to_date('2018-11-11','yyyy-mm-dd'),'10.119.130.250',2);
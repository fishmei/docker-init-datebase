#!/usr/bin/env bash
db=db
/mnt/bin/wait-for.sh ${db}:8080 -s -t 60 -- echo "oracle ${db} is up"
basesqldir=/mnt/sql
echo "connect successfully"
for sqlfile in `find ${basesqldir}/ -type f -iname '*.sql' | sort`
    do
        echo "SQL ${sqlfile} start executing...${db}"
        /u01/app/oracle/product/11.2.0/xe/bin/sqlplus  system/oracle@${db}:1521/xe @${sqlfile} >/dev/null 2>&1
        if [ 0 -eq $? ];then
            echo "SQL ${sqlfile} executed successfully"
        fi
    done

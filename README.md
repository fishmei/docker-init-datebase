# docker-init-datebase

#### 介绍
利用docker-compose初始化数据库，持续更新中

#### 软件架构
利用docker-compose启动开发数据库，在开发中保持数据库的一致性


#### 安装教程

1. 下载docker
2. 下载docker-compose

#### 使用说明

1. docker-compose up (启动，可增加-d参数，后台运行)
2. docker-compose build (构建)
3. docker-compose down 
